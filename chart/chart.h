#ifndef CHART_H
#define CHART_H

class PlotArea;
class Legend;

#include "../xml/node.h"

class Chart : public Node {
 public:
  Chart();

  void setResourceId(int rid);

  void setPlotArea(std::shared_ptr<PlotArea> plotArea);
  void setLegend(std::shared_ptr<Legend> legend);
  void setSize(int width, int height);

  // Node interface
 public:
  void printTo(std::ofstream &file) override;

 private:
  void printRelsTo(std::ofstream &file);
  void printChartTo(std::ofstream &file);

 private:
  int resourceId = 0;
  std::shared_ptr<PlotArea> plotArea;
  std::shared_ptr<Legend> legend;

  int width;
  int height;
};

#endif  // CHART_H
