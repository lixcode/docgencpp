#ifndef LEGEND_H
#define LEGEND_H

#include "axe.h"

#include "../xml/node.h"

class Legend : public Node {
 public:
  Legend();

  void setLegendPositition(Positions position);

  // Node interface
 public:
  void printTo(std::ofstream &file) override;

 private:
  Positions position;
};

#endif  // LEGEND_H
