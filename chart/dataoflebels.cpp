#include "dataoflebels.h"

DataOfLebels::DataOfLebels() {}

void DataOfLebels::setShowLegendKey(bool value) { showLegendKey = value; }

void DataOfLebels::setShowValue(bool value) { showValue = value; }

void DataOfLebels::setShowCategoryName(bool value) { showCategoryName = value; }

void DataOfLebels::setShowSerialName(bool value) { showSerialName = value; }

void DataOfLebels::setShowLeaderLines(bool value) { showLeaderLines = value; }

void DataOfLebels::printTo(std::ofstream &file) {
  file << R"(<c:dLbls>)";

  file << R"(<c:showLegendKey val=")" << int(showLegendKey) << R"(" />)";
  file << R"(<c:showVal val=")" << int(showValue) << R"(" />)";
  file << R"(<c:showCatName val=")" << int(showCategoryName) << R"(" />)";
  file << R"(<c:showSerName val=")" << int(showSerialName) << R"(" />)";
  file << R"(<c:showLeaderLines val=")" << int(showLeaderLines) << R"(" />)";

  file << R"(</c:dLbls>)";
}
