#ifndef AXE_H
#define AXE_H

#include "../xml/node.h"

class MajorGridLines;

enum class Positions { Left, Right, Top, Bottom };
enum class TickMarks { In, Out, None };
enum class AxeType { Category, Value };

std::string positionToString(Positions position);
std::string tickMarkToString(TickMarks tickMark);
std::string axeTypeToTagName(AxeType axeType);

class Axe : public Node {
 public:
  Axe(AxeType type);

  /**
   * @brief getType gets the type of axe
   * @return the type of axe
   */
  AxeType getType();

  /**
   * @brief getAxeId gets the axe id
   * @return the axe id
   */
  int getAxeId();

  /**
   * @brief setDelete set c:delete value
   * @param value is the value of delete property
   */
  void setDelete(bool value);

  /**
   * @brief setPosition set the postition of axe
   * @param position is top, left, right or bottom
   */
  void setPosition(Positions position);

  /**
   * @brief setMajorTickMark sets major tick marks
   * @param tickMark is the type of tick marks
   */
  void setMajorTickMark(TickMarks tickMark);

  /**
   * @brief setMinorTickMark sets mnor tick marks
   * @param tickMark is the type of tick marks
   */
  void setMinorTickMark(TickMarks tickMark);

  /**
   * @brief setMajorGridLine sets major grid lines
   * @param gridLines is the description of grid lines
   */
  void setMajorGridLine(std::shared_ptr<MajorGridLines> gridLines);

  /**
   * @brief setAxeColor sets the color of axe
   * @param color is a HEX color, for ex. 0x800012
   */
  void setAxeColor(RGB color);

  /**
   * @brief setOtherAxeId sets the another (value/category) axe id
   * @param axId is the id of category axe for value and id of value axe for cat axe
   */
  void setOtherAxeId(int axId);

  // Node interface
 public:
  void printTo(std::ofstream &file) override;

 private:
  AxeType type;
  int axeId = 0;
  bool delete_ = false;

  int otherAxeId = 0;

  Positions position = Positions::Bottom;
  TickMarks major = TickMarks::Out;
  TickMarks minor = TickMarks::None;

  RGB color = 0x878787;

  std::shared_ptr<MajorGridLines> majorGridLines = nullptr;

  // static
  static int lastId;
};

#endif  // AXE_H
