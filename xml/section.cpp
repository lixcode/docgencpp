#include "section.h"

Section::Section() {}

void Section::setPageSize(PageSize size) { this->size = std::move(size); }

void Section::setPageMargins(PageMargins margins) {
  this->margins = std::move(margins);
}

void Section::printTo(std::ofstream &file) {
  file << "<w:sectPr>";
  file << R"(<w:pgSz w:w=")" << size.width << R"(" w:h=")" << size.height
       << R"(" />)";
  file << R"(<w:pgMar w:top=")" << margins.top << R"(" w:right=")"
       << margins.right << R"(" w:bottom=")" << margins.bottom
       << R"(" w:left=")" << margins.left << R"(" w:header=")" << margins.header
       << R"(" w:footer=")" << margins.footer << R"(" w:gutter=")"
       << margins.gutter << R"(" />)";
  file << "</w:sectPr>";
}
