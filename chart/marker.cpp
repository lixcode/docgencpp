#include "marker.h"

std::string markToString(MarkSymbols mark) {
  std::string markAsSring;

  switch (mark) {
    case MarkSymbols::Square:
      markAsSring = "square";
      break;

    case MarkSymbols::Diamond:
      markAsSring = "diamond";
      break;

    case MarkSymbols::Triangle:
      markAsSring = "triangle";
      break;
  }

  return markAsSring;
}

Marker::Marker() {}

void Marker::setSymbol(MarkSymbols symbol) { this->symbol = symbol; }

void Marker::setSize(int size) { this->size = size; }

void Marker::setColor(RGB color) { this->color = color; }

RGB Marker::getColor() { return color; }

void Marker::printTo(std::ofstream &file) {
  file << R"(<c:marker>)";

  file << R"(<c:symbol val=")" << markToString(symbol).c_str() << R"(" />)";
  file << R"(<c:size val=")" << size << R"(" />)";
  file << R"(<c:spPr>)";
  file << R"(<a:solidFill>)";
  file << R"(<a:srgbClr val=")" << rgbToString(color).c_str() << R"(" />)";
  file << R"(</a:solidFill>)";
  file << R"(</c:spPr>)";

  file << R"(</c:marker>)";
}
