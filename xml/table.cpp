#include "table.h"
#include "row2.h"

Table::Table() {}

void Table::addRow(std::shared_ptr<Row2> row) { addChild(row); }

void Table::printPrefix(std::ofstream &file) { file << R"(<w:tbl>)"; }

void Table::printSuffix(std::ofstream &file) { file << R"(</w:tbl>)"; }
