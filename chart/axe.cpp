#include "axe.h"
#include "majorgridlines.h"

int Axe::lastId = 0;

std::string positionToString(Positions position) {
  std::string positionAsString;

  switch (position) {
    case Positions::Top:
      positionAsString = "t";
      break;

    case Positions::Right:
      positionAsString = "r";
      break;

    case Positions::Bottom:
      positionAsString = "b";
      break;

    case Positions::Left:
      positionAsString = "l";
      break;
  }

  return positionAsString;
}

std::string tickMarkToString(TickMarks tickMark) {
  std::string tickMarkAsString;

  switch (tickMark) {
    case TickMarks::In:
      tickMarkAsString = "in";
      break;

    case TickMarks::Out:
      tickMarkAsString = "out";
      break;

    case TickMarks::None:
      tickMarkAsString = "none";
      break;
  }

  return tickMarkAsString;
}

std::string axeTypeToTagName(AxeType axeType) {
  std::string tagName;

  switch (axeType) {
    case AxeType::Value:
      tagName = "valAx";
      break;

    case AxeType::Category:
      tagName = "catAx";
      break;
  }

  return tagName;
}

Axe::Axe(AxeType type) : type(type) { axeId = ++lastId; }

AxeType Axe::getType() { return type; }

int Axe::getAxeId() { return axeId; }

void Axe::setDelete(bool value) { delete_ = value; }

void Axe::setPosition(Positions position) { this->position = position; }

void Axe::setMajorTickMark(TickMarks tickMark) { major = tickMark; }

void Axe::setMinorTickMark(TickMarks tickMark) { minor = tickMark; }

void Axe::setMajorGridLine(std::shared_ptr<MajorGridLines> gridLines) {
  majorGridLines = gridLines;
}

void Axe::setAxeColor(RGB color) { this->color = color; }

void Axe::setOtherAxeId(int axId) { otherAxeId = axeId; }

void Axe::printTo(std::ofstream &file) {
  file << R"(<c:)" << axeTypeToTagName(type).c_str() << R"(>)";

  file << R"(<c:scaling>)";
  file << R"(<c:orientation val="minMax"/>)";
  file << R"(</c:scaling>)";

  file << R"(<c:axId val=")" << axeId << R"(" />)";
  file << R"(<c:delete val=")" << (delete_ ? 1 : 0) << R"(" />)";
  file << R"(<c:axPos val=")" << positionToString(position).c_str()
       << R"(" />)";
  file << R"(<c:majorTickMark val=")" << tickMarkToString(major).c_str()
       << R"(" />)";
  file << R"(<c:minorTickMark val=")" << tickMarkToString(minor).c_str()
       << R"(" />)";
  file << R"(<c:tickLblPos val="nextTo" />)";

  if (majorGridLines != nullptr) {
    majorGridLines->printTo(file);
  }

  file << R"(<c:spPr>)";
  file << R"(<a:ln>)";
  file << R"(<a:solidFill>)";
  file << R"(<a:srgbClr val=")" << rgbToString(color).c_str() << R"(" />)";
  file << R"(</a:solidFill>)";
  file << R"(</a:ln>)";
  file << R"(</c:spPr>)";

  file << "<c:txPr>"
       << "<a:bodyPr/>"
       << "<a:lstStyle/>"
       << "<a:p>"
       << "<a:pPr>"
       << "<a:defRPr>"
       << "<a:solidFill>"
       << R"(<a:srgbClr val="000000"/>)"
       << "</a:solidFill>"
       << "</a:defRPr>"
       << "</a:pPr>"
       << "</a:p>"
       << "</c:txPr>";

  file << R"(<c:crosses val="autoZero"/>)";
  file << R"(<c:crossAx val=")" << otherAxeId << R"(" />)";

  file << R"(</c:)" << axeTypeToTagName(type).c_str() << R"(>)";
}
