#ifndef NODE_H
#define NODE_H

#include <fstream>
#include <memory>

using RGB = int32_t;

std::string rgbToString(RGB rgb);

class Node {
 public:
  Node();
  virtual ~Node() = default;

  /**
   * @brief printTo prints node data to file
   * @param file must be opened and ready to write
   */
  virtual void printTo(std::ofstream &file) = 0;
};

#endif  // NODE_H
