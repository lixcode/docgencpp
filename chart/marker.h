#ifndef MARKER_H
#define MARKER_H

#include "../xml/node.h"

enum class MarkSymbols { Square, Diamond, Triangle };

std::string markToString(MarkSymbols mark);

class Marker : public Node {
 public:
  Marker();

  void setSymbol(MarkSymbols symbol);
  void setSize(int size);
  void setColor(RGB color);

  RGB getColor();

  // Node interface
 public:
  void printTo(std::ofstream &file) override;

 private:
  MarkSymbols symbol = MarkSymbols::Square;
  int size = 5;
  RGB color = 0x505050;
};

#endif  // MARKER_H
