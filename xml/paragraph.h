#ifndef PARAGRAPH_H
#define PARAGRAPH_H

#include "nodewithchildren.h"

class TextFragment;

struct Spacing {
  int above = 0;
  int after = 0;
};

class Paragraph : public NodeWithChildren {
 public:
  Paragraph();

  void setSpacing(Spacing spacing);
  void addTextFragment(std::shared_ptr<TextFragment> textFragment);

  // NodeWithChildren interface
 protected:
  void printPrefix(std::ofstream &file) override;
  void printSuffix(std::ofstream &file) override;

 private:
  Spacing spacing;
};

#endif  // PARAGRAPH_H
