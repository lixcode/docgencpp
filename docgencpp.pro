TEMPLATE = app
CONFIG += console c++17
CONFIG -= app_bundle
CONFIG -= qt

LIBS += -lstdc++fs -L$$PWD/../ziplib/Bin -lziplib

SOURCES += \
        main.cpp \
    xml/node.cpp \
    xml/nodewithchildren.cpp \
    xml/xmldocument.cpp \
    xml/paragraph.cpp \
    xml/textfragment.cpp \
    xml/table.cpp \
    xml/cell.cpp \
    xml/section.cpp \
    chart/chart.cpp \
    chart/legend.cpp \
    chart/plotarea.cpp \
    chart/axe.cpp \
    chart/linechart.cpp \
    chart/majorgridlines.cpp \
    chart/serial.cpp \
    chart/marker.cpp \
    chart/dataoflebels.cpp \
    xml/row2.cpp

HEADERS += \
    xml/node.h \
    xml/nodewithchildren.h \
    xml/xmldocument.h \
    xml/paragraph.h \
    xml/textfragment.h \
    xml/table.h \
    xml/cell.h \
    xml/section.h \
    chart/chart.h \
    chart/legend.h \
    chart/plotarea.h \
    chart/axe.h \
    chart/linechart.h \
    chart/majorgridlines.h \
    chart/serial.h \
    chart/marker.h \
    chart/dataoflebels.h \
    xml/row2.h
