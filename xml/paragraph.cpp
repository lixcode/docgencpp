#include "paragraph.h"
#include "textfragment.h"

Paragraph::Paragraph() {}

void Paragraph::setSpacing(Spacing spacing) { this->spacing = spacing; }

void Paragraph::addTextFragment(std::shared_ptr<TextFragment> textFragment) {
  addChild(textFragment);
}

void Paragraph::printPrefix(std::ofstream &file) {
  file << R"(<w:p>)";
  file << R"(<w:pPr>)";
  file << R"(<w:pStyle w:val="Normal"/>)";
  file << R"(<w:spacing w:before=")" << spacing.above << R"(" w:after=")"
       << spacing.after << R"(" />)";
  file << R"(<w:rPr/>)";
  file << R"(</w:pPr>)";
}

void Paragraph::printSuffix(std::ofstream &file) { file << R"(</w:p>)"; }
