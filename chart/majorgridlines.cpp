#include "majorgridlines.h"

MajorGridLines::MajorGridLines() {}

void MajorGridLines::setGridColor(RGB color) { this->color = color; }

void MajorGridLines::printTo(std::ofstream &file) {
  file << R"(<c:majorGridlines>)";

  file << R"(<c:spPr>)";
  file << R"(<a:ln>)";
  file << R"(<a:solidFill>)";
  file << R"(<a:srgbClr val=")" << rgbToString(color).c_str() << R"(" />)";
  file << R"(</a:solidFill>)";
  file << R"(</a:ln>)";
  file << R"(</c:spPr>)";

  file << R"(</c:majorGridlines>)";
}
