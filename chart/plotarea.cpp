#include "plotarea.h"
#include "axe.h"
#include "linechart.h"

PlotArea::PlotArea() {}

void PlotArea::setLineChart(std::shared_ptr<LineChart> chart) {
  this->chart = chart;
}

bool PlotArea::setCatAx(std::shared_ptr<Axe> axe) {
  categories = axe;

  if (chart != nullptr) {
    chart->addAxeId(axe->getAxeId());
  }

  if (values != nullptr) {
    values->setOtherAxeId(categories->getAxeId());
    categories->setOtherAxeId(values->getAxeId());
  }

  return chart != nullptr;
}

bool PlotArea::setValAx(std::shared_ptr<Axe> axe) {
  values = axe;

  if (chart != nullptr) {
    chart->addAxeId(axe->getAxeId());
  }

  if (categories != nullptr) {
    values->setOtherAxeId(categories->getAxeId());
    categories->setOtherAxeId(values->getAxeId());
  }

  return chart != nullptr;
}

void PlotArea::printTo(std::ofstream &file) {
  file << R"(<c:plotArea>)";

  if (chart != nullptr) chart->printTo(file);
  if (categories != nullptr) categories->printTo(file);
  if (values != nullptr) values->printTo(file);

  file << R"(</c:plotArea>)";
}
