#ifndef ROW2_H
#define ROW2_H

#include "nodewithchildren.h"

class Cell;

class Row2 : public NodeWithChildren {
 public:
  ~Row2() override = default;

  void addCell(std::shared_ptr<Cell> cell);

  // NodeWithChildren interface
 protected:
  void printPrefix(std::ofstream &file) override;
  void printSuffix(std::ofstream &file) override;
};

#endif  // ROW2_H
