#ifndef LINECHART_H
#define LINECHART_H

#include "../xml/node.h"
#include <list>

enum class GroupingModes { Standard };

class Serial;

std::string groupingToString(GroupingModes mode);

class LineChart : public Node {
 public:
  LineChart();

  void setGrouping(GroupingModes groupingMode);
  void setMarker(bool value);

  void addSerial(std::shared_ptr<Serial> serial);
  void addAxeId(int id);

  // Node interface
 public:
  void printTo(std::ofstream &file) override;

 private:
  GroupingModes groupingMode = GroupingModes::Standard;
  bool marker = true;

  std::list<std::shared_ptr<Serial>> serials;
  std::list<int> axesIds;
};

#endif  // LINECHART_H
