#ifndef TEXTFRAGMENT_H
#define TEXTFRAGMENT_H

#include "node.h"

class TextFragment : public Node {
 public:
  TextFragment();

  void setBold(bool value);
  void setItalic(bool value);
  void setText(const std::string& text);

  // Node interface
 public:
  void printTo(std::ofstream& file) override;

 private:
  std::string text;

  bool bold = false;
  bool italic = false;
};

#endif  // TEXTFRAGMENT_H
