#include "legend.h"

Legend::Legend() {}

void Legend::setLegendPositition(Positions position) {
  this->position = position;
}

void Legend::printTo(std::ofstream &file) {
  file << R"(<c:legend>)";
  file << R"(<c:legendPos val=")" << positionToString(position).c_str()
       << R"(" />)";
  file << R"(<c:overlay val="0"/>)";
  file << R"(</c:legend>)";
}
