#ifndef MAJORGRIDLINES_H
#define MAJORGRIDLINES_H

#include "../xml/node.h"

class MajorGridLines : public Node {
 public:
  MajorGridLines();

  void setGridColor(RGB color);

  // Node interface
 public:
  void printTo(std::ofstream &file) override;

 private:
  RGB color = 0x878787;
};

#endif  // MAJORGRIDLINES_H
