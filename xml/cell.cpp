#include "cell.h"
#include "paragraph.h"

std::string borderTypeToString(BorderType type) {
  std::string borderTypeAsString;

  switch (type) {
    case BorderType::None:
      borderTypeAsString = "none";
      break;

    case BorderType::Single:
      borderTypeAsString = "single";
      break;
  }

  return borderTypeAsString;
}

Cell::Cell() {}

void Cell::setBorder(BorderMargin margin, Border value) {
  if (int(margin) & int(BorderMargin::Top)) {
    top = value;
  }
  if (int(margin) & int(BorderMargin::Right)) {
    right = value;
  }
  if (int(margin) & int(BorderMargin::Bottom)) {
    bottom = value;
  }
  if (int(margin) & int(BorderMargin::Left)) {
    left = value;
  }
}

void Cell::setWidth(int width) { this->width = width; }

void Cell::addParagraph(std::shared_ptr<Paragraph> paragraph) {
  addChild(paragraph);
}

void Cell::printPrefix(std::ofstream &file) {
  file << "<w:tc>";
  file << "<w:tcPr>";
  file << R"(<w:tcW w:w=")" << width << R"(" w:type="dxa" />)";

  file << "<w:tcBorders>";

  if (top.type != BorderType::None) {
    file << R"(<w:top w:val=")" << borderTypeToString(top.type).c_str()
         << R"(" w:sz=")" << top.weight
         << R"(" w:space="0" w:color="000000" />)";
  }

  if (left.type != BorderType::None) {
    file << R"(<w:left w:val=")" << borderTypeToString(top.type).c_str()
         << R"(" w:sz=")" << left.weight
         << R"(" w:space="0" w:color="000000" />)";
  }

  if (bottom.type != BorderType::None) {
    file << R"(<w:bottom w:val=")" << borderTypeToString(top.type).c_str()
         << R"(" w:sz=")" << bottom.weight
         << R"(" w:space="0" w:color="000000" />)";
  }

  if (right.type != BorderType::None) {
    file << R"(<w:right w:val=")" << borderTypeToString(top.type).c_str()
         << R"(" w:sz=")" << right.weight
         << R"(" w:space="0" w:color="000000" />)";
  }

  file << "</w:tcBorders>";

  file << "</w:tcPr>";
}

void Cell::printSuffix(std::ofstream &file) { file << "</w:tc>"; }
