#include "textfragment.h"

TextFragment::TextFragment() {}

void TextFragment::setBold(bool value) { bold = value; }

void TextFragment::setItalic(bool value) { italic = value; }

void TextFragment::setText(const std::string &text) { this->text = text; }

void TextFragment::printTo(std::ofstream &file) {
  file << R"(<w:r>)";

  file << R"(<w:rPr>)";
  if (bold) file << "<w:b />";
  if (italic) file << "<w:i />";
  file << R"(</w:rPr>)";

  (file << R"(<w:t>)") << text.c_str() << R"(</w:t>)";
  file << R"(</w:r>)";
}
