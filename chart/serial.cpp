#include "serial.h"
#include "dataoflebels.h"
#include "marker.h"

int Serial::lastIdx = 0;

Serial::Serial(int order) {
  this->order = order;
  this->idx = lastIdx++;
}

void Serial::setLebel(std::string text) { label = text; }

void Serial::setLineWeight(int weight) { lineWeight = weight; }

void Serial::setMarker(std::shared_ptr<Marker> marker) {
  this->marker = marker;
}

void Serial::setDataOfLabels(std::shared_ptr<DataOfLebels> dol) {
  labelsData = dol;
}

void Serial::addData(const ChartData &data) { serialData.push_back(data); }

void Serial::printTo(std::ofstream &file) {
  file << R"(<c:ser>)";

  file << R"(<c:idx val=")" << idx << R"(" />)";
  file << R"(<c:order val=")" << order << R"(" />)";
  file << R"(<c:tx>)";
  file << R"(<c:strRef>)";
  file << R"(<c:f>label )" << order << R"(</c:f>)";
  file << R"(<c:strCache>)";
  file << R"(<c:ptCount val="1" />)";
  file << R"(<c:pt idx="0">)";
  file << R"(<c:v>)" << label.c_str() << R"(</c:v>)";
  file << R"(</c:pt>)";
  file << R"(</c:strCache>)";
  file << R"(</c:strRef>)";
  file << R"(</c:tx>)";
  file << R"(<c:spPr>)";
  file << R"(<a:ln w=")" << lineWeight << R"(">)";
  file << R"(<a:solidFill>)";
  file << R"(<a:srgbClr val=")" << rgbToString(marker->getColor()).c_str()
       << R"(" />)";
  file << R"(</a:solidFill>)";
  file << R"(</a:ln>)";
  file << R"(</c:spPr>)";

  if (marker != nullptr) marker->printTo(file);
  if (labelsData != nullptr) labelsData->printTo(file);

  file << R"(<c:cat>)";
  file << R"(<c:strRef>)";
  file << R"(<c:f>categories</c:f>)";
  file << R"(<c:strCache>)";
  file << R"(<c:ptCount val=")" << serialData.size() << R"(" />)";

  int categoryId = 0;

  for (auto dataUnit : serialData) {
    file << R"(<c:pt idx=")" << categoryId++ << R"(">)";
    file << R"(<c:v>)" << dataUnit.category.c_str() << R"(</c:v>)";
    file << R"(</c:pt>)";
  }

  file << R"(</c:strCache>)";
  file << R"(</c:strRef>)";
  file << R"(</c:cat>)";
  file << R"(<c:val>)";
  file << R"(<c:numRef>)";
  file << R"(<c:f>)" << order << R"(</c:f>)";
  file << R"(<c:numCache>)";
  file << R"(<c:formatCode>General</c:formatCode>)";
  file << R"(<c:ptCount val=")" << serialData.size() << R"(" />)";

  int valueId = 0;

  for (auto dataUnit : serialData) {
    file << R"(<c:pt idx=")" << valueId++ << R"(">)";
    file << R"(<c:v>)" << dataUnit.value << R"(</c:v>)";
    file << R"(</c:pt>)";
  }

  file << R"(</c:numCache>)";
  file << R"(</c:numRef>)";
  file << R"(</c:val>)";

  file << R"(<c:smooth val="0" />)";

  file << R"(</c:ser>)";
}
