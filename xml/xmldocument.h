#ifndef XMLDOCUMENT_H
#define XMLDOCUMENT_H

#include "nodewithchildren.h"

class Paragraph;
class Table;
class Chart;

class ZipArchive;

class XmlDocument : public NodeWithChildren {
 public:
  XmlDocument();

  void addParagraph(std::shared_ptr<Paragraph> paragraph);
  void addTable(std::shared_ptr<Table> table);
  void addChart(std::shared_ptr<Chart> chart);

  void createFilesOnDisk();

 private:
  void printRelsTo(std::ofstream &file);
  void printContentTypeTo(std::ofstream &file);
  void printDocumentRelsTo(std::ofstream &file);

  void createDocx();

  void archiveDir(const std::string &prefix,
                  std::shared_ptr<ZipArchive> &archive);

  // NodeWithChildren interface
 protected:
  void printPrefix(std::ofstream &file) override;
  void printSuffix(std::ofstream &file) override;

 private:
  static int lastResourceId;

  std::list<int> charts;
};

#endif  // XMLDOCUMENT_H
