#ifndef SERIAL_H
#define SERIAL_H

#include "../xml/node.h"
#include <list>

class Marker;
class DataOfLebels;

struct ChartData {
  std::string category;
  double value;
};

class Serial : public Node {
 public:
  Serial(int order);

  void setLebel(std::string text);
  void setLineWeight(int weight);
  void setMarker(std::shared_ptr<Marker> marker);
  void setDataOfLabels(std::shared_ptr<DataOfLebels> dol);
  void addData(const ChartData& data);

  // Node interface
 public:
  void printTo(std::ofstream& file) override;

 private:
  std::string label;
  int lineWeight = 28800;
  int order;
  int idx;
  std::shared_ptr<Marker> marker;
  std::shared_ptr<DataOfLebels> labelsData;
  std::list<ChartData> serialData;

  static int lastIdx;
};

#endif  // SERIAL_H
