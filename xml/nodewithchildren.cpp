#include "nodewithchildren.h"

NodeWithChildren::NodeWithChildren() {}

void NodeWithChildren::printTo(std::ofstream &file) {
  printPrefix(file);

  for (auto &childPtr : children) {
    childPtr->printTo(file);
  }

  printSuffix(file);
}

void NodeWithChildren::addChild(std::shared_ptr<Node> child) {
  children.push_back(child);
}
