#ifndef DATAOFLEBELS_H
#define DATAOFLEBELS_H

#include "../xml/node.h"

class DataOfLebels : public Node {
 public:
  DataOfLebels();

  void setShowLegendKey(bool value);
  void setShowValue(bool value);
  void setShowCategoryName(bool value);
  void setShowSerialName(bool value);
  void setShowLeaderLines(bool value);

  // Node interface
 public:
  void printTo(std::ofstream &file) override;

 private:
  bool showLegendKey = false;
  bool showValue = false;
  bool showCategoryName = false;
  bool showSerialName = false;
  bool showLeaderLines = false;
};

#endif  // DATAOFLEBELS_H
