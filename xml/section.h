#ifndef SECTION_H
#define SECTION_H

#include "node.h"

struct PageSize {
  int width;
  int height;
};

struct PageMargins {
  int top;
  int right;
  int bottom;
  int left;
  int header;
  int footer;
  int gutter;
};

class Section : public Node {
 public:
  Section();

  void setPageSize(PageSize size);
  void setPageMargins(PageMargins margins);

  // Node interface
 public:
  void printTo(std::ofstream &file) override;

 private:
  PageSize size;
  PageMargins margins;
};

#endif  // SECTION_H
