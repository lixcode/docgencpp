#include <iostream>

#include "xml/cell.h"
#include "xml/paragraph.h"
#include "xml/row2.h"
#include "xml/table.h"
#include "xml/textfragment.h"
#include "xml/xmldocument.h"

#include "chart/chart.h"
#include "chart/legend.h"
#include "chart/linechart.h"
#include "chart/majorgridlines.h"
#include "chart/marker.h"
#include "chart/plotarea.h"
#include "chart/serial.h"

using namespace std;

int main() {
  XmlDocument doc;

  // Test

  auto firstParagraph = std::make_shared<Paragraph>();
  auto firstText = std::make_shared<TextFragment>();

  firstText->setText("Test");
  firstParagraph->addTextFragment(firstText);
  doc.addParagraph(firstParagraph);

  // Тест

  auto secondParagraph = std::make_shared<Paragraph>();
  auto secondText = std::make_shared<TextFragment>();

  secondText->setText("Тест");
  secondParagraph->addTextFragment(secondText);
  secondParagraph->setSpacing({170, 170});
  doc.addParagraph(secondParagraph);

  // Table

  auto table = std::make_shared<Table>();

  auto row1 = std::make_shared<Row2>();

  // Bold

  auto boldCell = std::make_shared<Cell>();
  auto boldParagraph = std::make_shared<Paragraph>();
  auto boldText = std::make_shared<TextFragment>();

  boldText->setText("Bold");
  boldText->setBold(true);
  boldParagraph->addTextFragment(boldText);
  boldCell->addParagraph(boldParagraph);
  boldCell->setWidth(3300);
  boldCell->setBorder(BorderMargin::All, {BorderType::Single, 4});
  row1->addCell(boldCell);

  // Normal

  auto normalCell = std::make_shared<Cell>();
  auto normalParagraph = std::make_shared<Paragraph>();
  auto normalText = std::make_shared<TextFragment>();

  normalText->setText("Normal");
  normalParagraph->addTextFragment(normalText);
  normalCell->addParagraph(normalParagraph);
  normalCell->setWidth(3300);
  normalCell->setBorder(BorderMargin::All, {BorderType::Single, 4});
  row1->addCell(normalCell);

  // Italic

  auto italicCell = std::make_shared<Cell>();
  auto italicParagraph = std::make_shared<Paragraph>();
  auto italicText = std::make_shared<TextFragment>();

  italicText->setText("Italic");
  italicText->setItalic(true);
  italicParagraph->addTextFragment(italicText);
  italicCell->addParagraph(italicParagraph);
  italicCell->setWidth(3300);
  italicCell->setBorder(BorderMargin::All, {BorderType::Single, 4});
  row1->addCell(italicCell);

  table->addRow(row1);

  // the 2nd and 4rd rows

  for (int i = 0; i < 2; i++) {
    auto row = std::make_shared<Row2>();

    for (int j = 0; j < 3; j++) {
      auto cell = std::make_shared<Cell>();
      auto paragraph = std::make_shared<Paragraph>();
      auto fragment = std::make_shared<TextFragment>();

      row->addCell(cell);
      cell->addParagraph(paragraph);
      paragraph->addTextFragment(fragment);

      cell->setWidth(3300);
      cell->setBorder(BorderMargin::All, {BorderType::Single, 4});
    }

    table->addRow(row);
  }

  doc.addTable(table);

  // Add an empty paragraph

  auto emptyParagraph = std::make_shared<Paragraph>();
  auto emptyText = std::make_shared<TextFragment>();

  emptyParagraph->addTextFragment(emptyText);
  doc.addParagraph(emptyParagraph);

  // Chart

  auto chart = std::make_shared<Chart>();
  auto plotArea = std::make_shared<PlotArea>();
  auto legend = std::make_shared<Legend>();

  chart->setPlotArea(plotArea);
  chart->setLegend(legend);

  legend->setLegendPositition(Positions::Right);

  auto lineChart = std::make_shared<LineChart>();
  auto catAx = std::make_shared<Axe>(Axe{AxeType::Category});
  auto valAx = std::make_shared<Axe>(Axe{AxeType::Value});

  plotArea->setLineChart(lineChart);
  plotArea->setCatAx(catAx);
  plotArea->setValAx(valAx);

  auto hGrigLines = std::make_shared<MajorGridLines>();

  valAx->setPosition(Positions::Left);
  valAx->setMajorGridLine(hGrigLines);
  catAx->setPosition(Positions::Bottom);

  lineChart->setMarker(true);

  // Blue line

  auto blue = std::make_shared<Serial>(Serial{0});
  auto blueMarker = std::make_shared<Marker>();

  blue->setLebel("Text 1");
  blue->setMarker(blueMarker);
  blueMarker->setColor(0x4A7EBB);
  blueMarker->setSymbol(MarkSymbols::Square);

  // Red line

  auto red = std::make_shared<Serial>(Serial{1});
  auto redMarker = std::make_shared<Marker>();

  red->setLebel("Series 2");
  red->setMarker(redMarker);
  redMarker->setColor(0xBE4B48);
  redMarker->setSymbol(MarkSymbols::Diamond);

  // Green line

  auto green = std::make_shared<Serial>(Serial{2});
  auto greenMarker = std::make_shared<Marker>();

  green->setLebel("Series 3");
  green->setMarker(greenMarker);
  greenMarker->setColor(0x98B855);
  greenMarker->setSymbol(MarkSymbols::Triangle);

  // Content

  std::shared_ptr<Serial> serials[3] = {blue, red, green};
  std::string categories[4] = {"Category 1", "Category 2", "Category 3",
                               "Category 4"};
  double data[3][4] = {
      {4.3, 2.5, 3.5, 4.5}, {2.4, 4.4, 1.8, 2.8}, {2.0, 2.0, 3.0, 5.0}};

  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 4; j++) {
      serials[i]->addData({categories[j], data[i][j]});
    }

    lineChart->addSerial(serials[i]);
  }

  doc.addChart(chart);

  // Extracted from a read document
  chart->setSize(5486400, 3200400);

  doc.createFilesOnDisk();

  std::cout << "ready" << std::endl;

  return 0;
}
