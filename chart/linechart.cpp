#include "linechart.h"

#include "serial.h"

std::string groupingToString(GroupingModes mode) {
  std::string groupingAsString;

  switch (mode) {
    case GroupingModes::Standard:
      groupingAsString = "standard";
      break;
  }

  return groupingAsString;
}

LineChart::LineChart() {}

void LineChart::setGrouping(GroupingModes groupingMode) {
  this->groupingMode = groupingMode;
}

void LineChart::setMarker(bool value) { this->marker = value; }

void LineChart::addSerial(std::shared_ptr<Serial> serial) {
  serials.push_back(serial);
}

void LineChart::addAxeId(int id) { axesIds.push_back(id); }

void LineChart::printTo(std::ofstream &file) {
  file << R"(<c:lineChart>)";

  file << R"(<c:grouping val=")" << groupingToString(groupingMode).c_str()
       << R"(" />)";
  file << R"(<c:marker val=")" << int(marker) << R"(" />)";

  for (auto &serial : serials) {
    serial->printTo(file);
  }

  for (auto &axeId : axesIds) {
    file << R"(<c:axId val=")" << axeId << R"(" />)";
  }

  file << R"(</c:lineChart>)";
}
