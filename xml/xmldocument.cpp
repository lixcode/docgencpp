#include "xmldocument.h"
#include "paragraph.h"
#include "table.h"

#include <filesystem>
#include <list>
#include "../chart/chart.h"

#include "../ziplib/ZipArchiveEntry.h"
#include "../ziplib/ZipFile.h"

int XmlDocument::lastResourceId = 0;

XmlDocument::XmlDocument() {}

void XmlDocument::addParagraph(std::shared_ptr<Paragraph> paragraph) {
  addChild(paragraph);
}

void XmlDocument::addTable(std::shared_ptr<Table> table) { addChild(table); }

void XmlDocument::addChart(std::shared_ptr<Chart> chart) {
  addChild(chart);

  int resourceId = ++lastResourceId;
  chart->setResourceId(resourceId);
  charts.push_back(resourceId);
}

void XmlDocument::createFilesOnDisk() {
  std::filesystem::create_directory("docx");
  std::filesystem::create_directory("docx/word");
  std::filesystem::create_directory("docx/_rels");
  std::filesystem::create_directory("docx/word/_rels");

  std::ofstream rels("docx/_rels/.rels");

  if (rels.is_open()) {
    printRelsTo(rels);
  }

  std::ofstream contentTypes("docx/[Content_Types].xml");

  if (rels.is_open()) {
    printContentTypeTo(contentTypes);
  }

  std::ofstream document("docx/word/document.xml");

  if (document.is_open()) {
    printTo(document);
  }

  std::ofstream docRels("docx/word/_rels/document.xml.rels");

  if (docRels.is_open()) {
    printDocumentRelsTo(docRels);
  }

  rels.close();
  contentTypes.close();
  document.close();
  docRels.close();

  createDocx();
}

void XmlDocument::printRelsTo(std::ofstream &file) {
  file << "<?xml version=\"1.0\" encoding=\"UTF-8\" "
          "standalone=\"yes\"?>\n<Relationships "
          "xmlns=\"http://schemas.openxmlformats.org/package/2006/"
          "relationships\"><Relationship Id=\"rId1\" "
          "Type=\"http://schemas.openxmlformats.org/officeDocument/2006/"
          "relationships/officeDocument\" Target=\"word/document.xml\" "
          "/></Relationships>";
}

void XmlDocument::printContentTypeTo(std::ofstream &file) {
  file << "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>";
  file << "<Types "
          "xmlns=\"http://schemas.openxmlformats.org/package/2006/"
          "content-types\">";
  file << "<Default Extension=\"rels\" "
          "ContentType=\"application/"
          "vnd.openxmlformats-package.relationships+xml\" />";
  file << "<Default Extension=\"xml\" ContentType=\"application/xml\" />";
  file << "<Override "
          "ContentType=\"application/"
          "vnd.openxmlformats-officedocument.wordprocessingml.styles+xml\" "
          "PartName=\"/word/styles.xml\"/>";
  file << "<Override PartName=\"/word/document.xml\" "
          "ContentType=\"application/"
          "vnd.openxmlformats-officedocument.wordprocessingml.document.main+"
          "xml\" />";

  for (auto &chart : charts) {
    file << "<Override PartName=\"/word/charts/chart" << chart
         << ".xml\" ContentType=\"application/"
            "vnd.openxmlformats-officedocument.drawingml.chart+xml\" />";
  }

  file << "</Types>";
}

void XmlDocument::printDocumentRelsTo(std::ofstream &file) {
  file << R"(<?xml version="1.0" encoding="UTF-8" standalone="yes"?>)";
  file << "\n<Relationships "
          "xmlns=\"http://schemas.openxmlformats.org/package/2006/"
          "relationships\">";

  for (auto &chart : charts) {
    file << "<Relationship Id=\"rId" << chart
         << "\" Type=\"http://schemas.openxmlformats.org/officeDocument/2006/"
            "relationships/chart\" Target=\"charts/chart"
         << chart << ".xml\" />";
  }

  file << "<Relationship Id=\"rId" << (lastResourceId + 1)
       << "\" Target=\"styles.xml\" "
          "Type=\"http://schemas.openxmlformats.org/officeDocument/2006/"
          "relationships/styles\"/>";

  file << "</Relationships>";
}

void XmlDocument::createDocx() {
  std::filesystem::remove("out.docx");

  ZipArchive::Ptr docx = ZipFile::Open("out.docx");

  archiveDir("", docx);

  ZipFile::SaveAndClose(docx, "out.docx");
}

void XmlDocument::archiveDir(const std::string &prefix,
                             ZipArchive::Ptr &archive) {
  for (auto &entry : std::filesystem::directory_iterator("docx/" + prefix)) {
    if (entry.is_directory()) {
      archiveDir(prefix + "/" + entry.path().filename().string(), archive);
    } else if (entry.is_regular_file()) {
      // Compress the file now
      std::ifstream stream(entry.path().native(), std::ios::binary);
      ZipArchiveEntry::Ptr archiveEntry =
          archive->CreateEntry(prefix + "/" + entry.path().filename().string());

      // use deflate method
      archiveEntry->SetCompressionStream(
          stream, DeflateMethod::Create(),
          ZipArchiveEntry::CompressionMode::Immediate);
    }
  }
}

void XmlDocument::printPrefix(std::ofstream &file) {
  file << R"(<?xml version="1.0" encoding="UTF-8"?>)";
  file << "<w:document "
          "xmlns:w=\"http://schemas.openxmlformats.org/wordprocessingml/2006/"
          "main\" "
          "xmlns:m=\"http://schemas.openxmlformats.org/officeDocument/2006/"
          "math\" "
          "xmlns:mc=\"http://schemas.openxmlformats.org/markup-compatibility/"
          "2006\" xmlns:o=\"urn:schemas-microsoft-com:office:office\" "
          "xmlns:r=\"http://schemas.openxmlformats.org/officeDocument/2006/"
          "relationships\" xmlns:v=\"urn:schemas-microsoft-com:vml\" "
          "xmlns:w10=\"urn:schemas-microsoft-com:office:word\" "
          "xmlns:w14=\"http://schemas.microsoft.com/office/word/2010/wordml\" "
          "xmlns:wne=\"http://schemas.microsoft.com/office/word/2006/wordml\" "
          "xmlns:wp=\"http://schemas.openxmlformats.org/drawingml/2006/"
          "wordprocessingDrawing\" "
          "xmlns:wp14=\"http://schemas.microsoft.com/office/word/2010/"
          "wordprocessingDrawing\" "
          "xmlns:wpc=\"http://schemas.microsoft.com/office/word/2010/"
          "wordprocessingCanvas\" "
          "xmlns:wpg=\"http://schemas.microsoft.com/office/word/2010/"
          "wordprocessingGroup\" "
          "xmlns:wpi=\"http://schemas.microsoft.com/office/word/2010/"
          "wordprocessingInk\" "
          "xmlns:wps=\"http://schemas.microsoft.com/office/word/2010/"
          "wordprocessingShape\" mc:Ignorable=\"w14 wp14\">";
  file << R"(<w:body>)";
}

void XmlDocument::printSuffix(std::ofstream &file) {
  file << R"(</w:body>)";
  file << R"(</w:document>)";
}
