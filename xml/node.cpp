#include "node.h"

char hexDigitToChar(int8_t hexDigit) {
  char result;

  if (hexDigit >= 0 && hexDigit <= 9) {
    result = '0' + hexDigit;
  } else {
    result = 'A' + hexDigit - 10;
  }

  return result;
}

std::string rgbToString(RGB rgb) {
  std::string rgbAsString = "000000";

  for (unsigned int i = 0; i < 6; i++) {
    rgbAsString[5u - i] = hexDigitToChar(rgb & 0x0f);
    rgb = rgb >> 4;
  }

  return rgbAsString;
}

Node::Node() {}
