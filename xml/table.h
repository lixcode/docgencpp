#ifndef TABLE_H
#define TABLE_H

#include "nodewithchildren.h"

class Row2;

class Table : public NodeWithChildren {
 public:
  Table();

  void addRow(std::shared_ptr<Row2> row);

  // NodeWithChildren interface
 protected:
  void printPrefix(std::ofstream &file) override;
  void printSuffix(std::ofstream &file) override;
};

#endif  // TABLE_H
