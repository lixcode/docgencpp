#ifndef CELL_H
#define CELL_H

#include "nodewithchildren.h"

enum class BorderMargin {
  None = 0,
  Top = 1,
  Right = 2,
  Bottom = 4,
  Left = 8,
  All = 0xF
};

enum class BorderType { None, Single };

struct Border {
  BorderType type = BorderType::None;
  int weight = 0;
};

class Paragraph;

std::string borderTypeToString(BorderType type);

class Cell : public NodeWithChildren {
 public:
  Cell();

  void setBorder(BorderMargin margin, Border value);
  void setWidth(int width);
  void addParagraph(std::shared_ptr<Paragraph> paragraph);

 private:
  Border top;
  Border left;
  Border right;
  Border bottom;

  int width;

  // NodeWithChildren interface
 protected:
  void printPrefix(std::ofstream &file) override;
  void printSuffix(std::ofstream &file) override;
};

#endif  // CELL_H
