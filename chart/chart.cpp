#include "chart.h"
#include "legend.h"
#include "plotarea.h"

#include <filesystem>

Chart::Chart() {}

void Chart::setResourceId(int rid) { resourceId = rid; }

void Chart::setPlotArea(std::shared_ptr<PlotArea> plotArea) {
  this->plotArea = plotArea;
}

void Chart::setLegend(std::shared_ptr<Legend> legend) { this->legend = legend; }

void Chart::setSize(int width, int height) {
  this->width = width;
  this->height = height;
}

void Chart::printTo(std::ofstream &file) {
  file << R"(<w:p>)";
  file << R"(<w:r>)";
  file << R"(<w:drawing>)";
  file << R"(<wp:inline distT="0" distB="0" distL="0" distR="0">)";
  file << R"(<wp:extent cx=")" << width << R"(" cy=")" << height << R"(" />)";
  file << R"(<wp:docPr id="1" name=""/>)";
  file << "<a:graphic "
          "xmlns:a=\"http://schemas.openxmlformats.org/drawingml/2006/main\">";
  file << "<a:graphicData "
          "uri=\"http://schemas.openxmlformats.org/drawingml/2006/chart\">";
  file << "<c:chart "
          "xmlns:c=\"http://schemas.openxmlformats.org/drawingml/2006/chart\" "
          "r:id=\"rId"
       << resourceId
       << "\" "
          "xmlns:r=\"http://schemas.openxmlformats.org/officeDocument/2006/"
          "relationships\" />";
  file << R"(</a:graphicData>)";
  file << R"(</a:graphic>)";
  file << R"(</wp:inline>)";
  file << R"(</w:drawing>)";
  file << R"(</w:r>)";
  file << R"(</w:p>)";

  std::string filename = "chart" + std::to_string(resourceId) + ".xml";

  std::filesystem::create_directory("docx/word/charts");
  std::filesystem::create_directory("docx/word/charts/_rels");

  std::ofstream rels("docx/word/charts/_rels/" + filename + ".rels");

  if (rels.is_open()) {
    printRelsTo(rels);
  }

  rels.close();

  std::ofstream chartFile("docx/word/charts/" + filename);

  if (chartFile.is_open()) {
    printChartTo(chartFile);
  }

  chartFile.close();
}

void Chart::printRelsTo(std::ofstream &file) {
  file << R"(<?xml version="1.0" encoding="UTF-8" standalone="yes"?>)";
  file << "<Relationships "
          "xmlns=\"http://schemas.openxmlformats.org/package/2006/"
          "relationships\">";
  file << R"(</Relationships>)";
}

void Chart::printChartTo(std::ofstream &file) {
  file << R"(<?xml version="1.0" encoding="UTF-8" standalone="yes"?>)";
  file << "<c:chartSpace "
          "xmlns:c=\"http://schemas.openxmlformats.org/drawingml/2006/chart\" "
          "xmlns:a=\"http://schemas.openxmlformats.org/drawingml/2006/main\" "
          "xmlns:r=\"http://schemas.openxmlformats.org/officeDocument/2006/"
          "relationships\">";
  file << R"(<c:roundedCorners val="0"/>)";
  file << R"(<c:chart>)";

  if (plotArea != nullptr) plotArea->printTo(file);
  if (legend != nullptr) legend->printTo(file);

  file << R"(</c:chart>)";
  file << R"(</c:chartSpace>)";
}
