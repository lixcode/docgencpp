#ifndef PLOTAREA_H
#define PLOTAREA_H

#include "../xml/node.h"
#include <list>

class LineChart;
class Axe;

class PlotArea : public Node {
 public:
  PlotArea();

  void setLineChart(std::shared_ptr<LineChart> chart);
  bool setCatAx(std::shared_ptr<Axe> axe);
  bool setValAx(std::shared_ptr<Axe> axe);

  // Node interface
 public:
  void printTo(std::ofstream &file) override;

 private:
  std::shared_ptr<LineChart> chart;
  std::shared_ptr<Axe> categories;
  std::shared_ptr<Axe> values;
};

#endif  // PLOTAREA_H
