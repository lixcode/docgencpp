#ifndef NODEWITHCHILDREN_H
#define NODEWITHCHILDREN_H

#include "node.h"

#include <list>

class NodeWithChildren : public Node {
 public:
  NodeWithChildren();

  // Node interface
 public:
  void printTo(std::ofstream &file) override;

 protected:
  /**
   * @brief addChild add a children node
   * @param child is the node to add
   */
  void addChild(std::shared_ptr<Node> child);

  /**
   * @brief printPrefix prints the open tag, for ex. <w:p>
   * @param file is the file to print in
   */
  virtual void printPrefix(std::ofstream &file) = 0;

  /**
   * @brief printSuffix prints the close tag, for ex. </w:p>
   * @param file is the file to print in
   */
  virtual void printSuffix(std::ofstream &file) = 0;

 private:
  /**
   * @brief children are the children of this node
   */
  std::list<std::shared_ptr<Node>> children;
};

#endif  // NODEWITHCHILDREN_H
