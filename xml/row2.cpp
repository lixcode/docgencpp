#include "row2.h"
#include "cell.h"

void Row2::addCell(std::shared_ptr<Cell> cell) { addChild(cell); }

void Row2::printPrefix(std::ofstream &file) { file << "<w:tr>"; }

void Row2::printSuffix(std::ofstream &file) { file << "</w:tr>"; }
